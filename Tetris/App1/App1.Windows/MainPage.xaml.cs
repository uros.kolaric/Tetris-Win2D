﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI;
using Microsoft.Graphics.Canvas.UI.Xaml;
using Microsoft.Graphics.Canvas;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Tetris
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private TetrisGame game;

        public MainPage()
        {
            this.InitializeComponent();

            game = new TetrisGame(20,30);
            canvasAnimatedControl.TargetElapsedTime = TimeSpan.FromSeconds(0.4);
        }

        private void canvasAnimatedControl_Update(ICanvasAnimatedControl sender, CanvasAnimatedUpdateEventArgs args)
        {
            game.Update();
        }

        private void canvasAnimatedControl_Draw(ICanvasAnimatedControl sender, CanvasAnimatedDrawEventArgs args)
        {
            game.Draw(args.DrawingSession, sender.Size);
        }

        private void right_Click(object sender, RoutedEventArgs e)
        {
            var action = this.canvasAnimatedControl.RunOnGameLoopThreadAsync(() =>
                game.MainElementMove(1, 0));
        }

        private void left_Click(object sender, RoutedEventArgs e)
        {
            var action = this.canvasAnimatedControl.RunOnGameLoopThreadAsync(() =>
                game.MainElementMove(-1, 0));
        }

        private void rotate_Click(object sender, RoutedEventArgs e)
        {
            var action = this.canvasAnimatedControl.RunOnGameLoopThreadAsync(() =>
                game.MainElementRotate());
        }
    }
}
