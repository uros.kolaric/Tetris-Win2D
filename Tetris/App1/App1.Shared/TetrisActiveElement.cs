﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace Tetris
{
    class TetrisActiveElement
    {
        private int elementRotatePosition;
        private Point elementStartPoint;
        private bool[,] elementField;        
        private TetrisElementType elementType;
        private Size elementSize;
        private Color elementColor;
        private List<Point> elementActiveFields;

        public TetrisActiveElement(TetrisActiveElement element)
        {
            elementRotatePosition = element.elementRotatePosition;
            elementStartPoint = element.elementStartPoint;
            elementField = element.elementField;
            elementType = element.elementType;
            elementSize = element.elementSize;
            elementColor = element.elementColor;
            elementActiveFields = element.elementActiveFields;
        }

        public TetrisActiveElement(TetrisElementType tetrisElementType, Point startPoint)
        {
            Random rnd = new Random(DateTime.Now.Millisecond);
            this.elementRotatePosition = rnd.Next(4);
            this.elementType = tetrisElementType;
            this.elementField = GetActiveElementField();
            this.elementStartPoint = startPoint;
            this.elementActiveFields = GetElementActiveFields();
            this.elementColor = GetElementColor(this.elementType);
            if (tetrisElementType == TetrisElementType.I)
                this.elementSize = new Size(4, 4);
            else if (tetrisElementType == TetrisElementType.O)
                this.elementSize = new Size(4, 3);
            else
                this.elementSize = new Size(3, 3);
        }

        public List<Point> ElementActiveFields
        {
            get
            {
                return elementActiveFields;
            }
        }

        private static Color GetElementColor(TetrisElementType elementType)
        {
            switch(elementType)
            {
                case TetrisElementType.L:
                    return Colors.Gold;
                case TetrisElementType.J:
                    return Colors.Blue;
                case TetrisElementType.I:
                    return Colors.Orange;
                case TetrisElementType.O:
                    return Colors.Yellow;
                case TetrisElementType.Z:
                    return Colors.Green;
                case TetrisElementType.S:
                    return Colors.Purple;
                case TetrisElementType.T:
                    return Colors.BlueViolet;
                default:
                    return Colors.Aqua;
            }
        }

        //TODO update return, this is not nice code
        private bool[,] GetActiveElementField()
        {
            bool[,] filed = new bool[0,0];
            switch (elementType)
            {
                case TetrisElementType.L:
                    switch (elementRotatePosition)
                    {
                        case 0:
                            filed = new bool[,] { { false, false, false }, { true, false, false }, { true, true, true } };
                            break;
                        case 1:
                            filed = new bool[,] { { true, true, false }, { true, false, false }, { true, false, false } };
                            break;
                        case 2:
                            filed = new bool[,] { { true, true, true }, { false, false, true }, { false, false, false } };
                            break;
                        case 3:
                            filed = new bool[,] { { false, false, true }, { false, false, true }, { false, true, true } };
                            break;
                    }
                    break;
                case TetrisElementType.J:
                    switch (elementRotatePosition)
                    {
                        case 0:
                            filed = new bool[,] { { false, false, false }, { false, false, true }, { true, true, true } };
                            break;
                        case 1:
                            filed = new bool[,] { { true, false, false }, { true, false, false }, { true, true, false } };
                            break;
                        case 2:
                            filed = new bool[,] { { true, true, true }, { true, false, false }, { false, false, false } };
                            break;
                        case 3:
                            filed = new bool[,] { { false, true, true }, { false, false, true }, { false, false, true } };
                            break;
                    }
                    break;
                case TetrisElementType.Z:
                    switch (elementRotatePosition)
                    {
                        case 0:
                            filed = new bool[,] { { true, true, false }, { false, true, true }, { false, false, false } };
                            break;
                        case 1:
                            filed = new bool[,] { { false, false, true }, { false, true, true }, { false, true, false } };
                            break;
                        case 2:
                            filed = new bool[,] { { false, false, false }, { true, true, false }, { false, true, true } };
                            break;
                        case 3:
                            filed = new bool[,] { { false, true, false }, { true, true, false }, { true, false, false } };
                            break;
                    }
                    break;
                case TetrisElementType.S:
                    switch (elementRotatePosition)
                    {
                        case 0:
                            filed = new bool[,] { { false, true, true }, { true, true, false }, { false, false, false } };
                            break;
                        case 1:
                            filed = new bool[,] { { false, true, false }, { false, true, true }, { false, false, true } };
                            break;
                        case 2:
                            filed = new bool[,] { { false, false, false }, { false, true, true }, { true, true, false } };
                            break;
                        case 3:
                            filed = new bool[,] { { true, false, false }, { true, true, false }, { false, true, false } };
                            break;
                    }
                    break;
                case TetrisElementType.T:
                    switch (elementRotatePosition)
                    {
                        case 0:
                            filed = new bool[,] { { false, true, false }, { true, true, true }, { false, false, false } };
                            break;
                        case 1:
                            filed = new bool[,] { { false, true, false }, { false, true, true }, { false, true, false } };
                            break;
                        case 2:
                            filed = new bool[,] { { false, false, false }, { true, true, true }, { false, true, false } };
                            break;
                        case 3:
                            filed = new bool[,] { { false, true, false }, { true, true, false }, { false, true, false } };
                            break;
                    }
                    break;
                case TetrisElementType.O:
                    filed = new bool[,] { { false, true, true, false }, { false, true, true, false }, { false, false, false, false } };
                    break;
                case TetrisElementType.I:
                    switch (elementRotatePosition)
                    {
                        case 0:
                            filed = new bool[,] { { false, false, false, false }, { false, false, false, false }, { true, true, true, true }, { false, false, false, false } };
                            break;
                        case 1:
                            filed = new bool[,] { { false, false, true, false }, { false, false, true, false }, { false, false, true, false }, { false, false, true, false } };
                            break;
                        case 2:
                            filed = new bool[,] { { false, false, false, false }, { false, false, false, false }, { true, true, true, true }, { false, false, false, false } };
                            break;
                        case 3:
                            filed = new bool[,] { { false, true, false, false }, { false, true, false, false }, { false, true, false, false }, { false, true, false, false } };
                            break;
                    }
                    break;
                case TetrisElementType.OO:
                    filed = new bool[,] { { true, true, true }, { true, false, true }, { true, true, true } };
                    break;
            }
            return filed;
        }

        public void Draw(CanvasDrawingSession ds, Size singleElementDrawSize)
        {
            for (int i = 0; i < elementSize.Height; i++)
                for (int j = 0; j < elementSize.Width; j++)               
                    if (elementField[i,j])
                    {
                        Rect rectangle = new Rect((elementStartPoint.X * singleElementDrawSize.Width) + (singleElementDrawSize.Width * j),
                            (elementStartPoint.Y * singleElementDrawSize.Height) + (singleElementDrawSize.Height * i), singleElementDrawSize.Width, singleElementDrawSize.Height);
                        ds.FillRectangle(rectangle, elementColor);
                    }
        }

        internal void Move(double x, double y)
        {
            elementStartPoint.X += x;
            elementStartPoint.Y += y;
            elementActiveFields = GetElementActiveFields();
        }

        internal void rotate()
        {
            elementRotatePosition = (elementRotatePosition + 1) % 4;
            elementField = GetActiveElementField();
            elementActiveFields = GetElementActiveFields();
        }

        private List<Point> GetElementActiveFields()
        {
            List<Point> listOfPoints = new List<Point>();

            for (int i = 0; i < elementSize.Height; i++)
                for (int j = 0; j < elementSize.Width; j++)
                    if (elementField[i, j])
                    {
                        listOfPoints.Add(new Point(elementStartPoint.X + j, elementStartPoint.Y + i));
                    }

            return listOfPoints;
        }
    }
}
