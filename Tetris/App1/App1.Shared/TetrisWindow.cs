﻿
using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace Tetris
{
    enum TetrisElementType { O, I, L, J, T, Z, S, OO};

    class TetrisWindow
    {
        private TetrisActiveElement activeElement;
        private TetrisGameField gameField;
        private int numberOfColumns;
        private int numberOfRows;

        public TetrisWindow(int numberOfColumns, int numberOfRows)
        {
            activeElement = new TetrisActiveElement(GetRandomTetrisElementType(), new Point(4, -3));
            gameField = new TetrisGameField(numberOfColumns, numberOfRows);

            this.numberOfColumns = numberOfColumns;
            this.numberOfRows = numberOfRows;
        }

        private static TetrisElementType GetRandomTetrisElementType()
        {
            Array values = Enum.GetValues(typeof(TetrisElementType));
            Random random = new Random(DateTime.Now.Millisecond);
            return (TetrisElementType)values.GetValue(random.Next(values.Length));
        }

        public void Update()
        {   
            if (gameField.IsElementAtEnd(activeElement.ElementActiveFields))
            {
                gameField.UpdateGameField(activeElement.ElementActiveFields);
                gameField.ClearFullLines();
                activeElement = new TetrisActiveElement(GetRandomTetrisElementType(), new Point(4, -3));
            }
            else
            {
                MainElementMove(0, 1);
            }
        }

        internal void Draw(CanvasDrawingSession ds, Size gameWindowSize)
        {
            Size gameElementSize = new Size(gameWindowSize.Width / numberOfColumns, gameWindowSize.Height / numberOfRows);
            activeElement.Draw(ds, gameElementSize);
            gameField.Draw(ds, gameElementSize);
            DrawGrid(ds, gameWindowSize);
        }

        void DrawGrid(CanvasDrawingSession ds, Size gameWindowSize)
        {
            float x = (float)gameWindowSize.Width / numberOfColumns;
            float y = (float)gameWindowSize.Height / numberOfRows;

            for (int i = 0; i < numberOfColumns; i++)
                ds.DrawLine(x * i, 0, x * i, (float)gameWindowSize.Height, Colors.Black);
            for (int i = 0; i < numberOfRows; i++)
                ds.DrawLine(0, y * i, (float)gameWindowSize.Width, y * i, Colors.Black);
        }

        internal void MainElementRotate()
        {
            TetrisActiveElement element = new TetrisActiveElement(activeElement);
            element.rotate();
            if (gameField.IsMoveLegal(element.ElementActiveFields, 0, 0))
                activeElement.rotate();
        }

        internal void MainElementMove(double factorX, double factorY)
        {
            if (gameField.IsMoveLegal(activeElement.ElementActiveFields, factorX, factorY))
                activeElement.Move(factorX, factorY);
        }        
    }
}
