﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace Tetris
{
    class TetrisGame
    {   
        private TetrisWindow gameWindow;
        static readonly object _object = new object();

        public TetrisGame(int numberOfColumns, int numberOfRows)
        {
            gameWindow = new TetrisWindow(numberOfColumns, numberOfRows);
        }

        internal void Draw(CanvasDrawingSession ds, Size gameWindowSize)
        {
            gameWindow.Draw(ds, gameWindowSize);
        }

        internal void Update()
        {            
            lock (_object)
            {                
                gameWindow.Update();
            }
        }      

        internal void MainElementMove(double factorX, double factorY)
        {
            lock (_object)
            {
                gameWindow.MainElementMove(factorX, factorY);
            }
        }

        internal void MainElementRotate()
        {
            lock (_object)
            {
                gameWindow.MainElementRotate();
            }
        }
    }
}
