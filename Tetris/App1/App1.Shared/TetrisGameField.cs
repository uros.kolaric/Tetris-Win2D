﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace Tetris
{
    class TetrisGameField
    {
        private bool[,] field;
        private int numberOfColumns;
        private int numberOfRows;

        public TetrisGameField(int numberOfColumns, int numberOfRows)
        {
            this.numberOfColumns = numberOfColumns;
            this.numberOfRows = numberOfRows;
            field = new bool[numberOfRows, numberOfColumns];//20*10
        }

        internal bool IsMoveLegal(List<Point> listOfPoints, double faktorX, double faktorY)
        {
            foreach (Point p in listOfPoints)
            {
                int x = (int)(p.X + faktorX);
                int y = (int)(p.Y + faktorY);

                if (y < 0)
                    return true;

                if (x < 0 || x >= numberOfColumns || field[y, x])
                    return false;
            }
            return true;
        }

        internal bool IsElementAtEnd(List<Point> listOfPoints)
        {
            foreach (Point p in listOfPoints)
            {
                int x = (int)p.X;
                int y = (int)p.Y + 1;

                if (y < 0)
                    return false;
                if (y == numberOfRows || field[y, x])
                    return true;
            }

            return false;
        }

        internal void Draw(CanvasDrawingSession ds, Size gameElementSize)
        {
            for (int i = 0; i < numberOfRows; i++)
                for (int j = 0; j < numberOfColumns; j++)
                    if (field[i, j])
                    {
                        Rect rectangle = new Rect(gameElementSize.Width * j, gameElementSize.Height * i,
                           gameElementSize.Width, gameElementSize.Height);
                        ds.FillRectangle(rectangle, Colors.Brown);
                    }
        }

        internal void UpdateGameField(List<Point> listOfPoints)
        {
            foreach (Point p in listOfPoints)
            {
                int x = (int)p.X;
                int y = (int)p.Y;

                field[y, x] = true;
            }
        }

        //TODO: update this 
        internal void ClearFullLines()
        {
            List<int> fullLines = new List<int>();
            int count;
            for (int i = 0; i < numberOfRows; i++)
            {
                count = 0;
                for (int j = 0; j < numberOfColumns; j++)
                    if (field[i, j])
                        count++;
                if (count == numberOfColumns)//full line                
                    fullLines.Add(i);
            }

            //handle full lines
            foreach (int line in fullLines)
            {
                //deleting full lines
                for (int j = 0; j < numberOfColumns; j++)
                    field[line, j] = false;

                //move the rest of lines
                for (int i = line; i > 1; i--)
                    for (int j = 0; j < numberOfColumns; j++)
                        if (field[i - 1, j])
                        {
                            field[i, j] = true;
                            field[i - 1, j] = false;
                        }
            }
        }
    }
}